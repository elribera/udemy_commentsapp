# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.context import RequestContext
from django.views.generic import View
from django.contrib.auth.models import User,Group
from udemy_task_two.modelforms import FormCreator
from django import forms
import simplejson
import datetime
from webapp.models import * 
import os
from django.conf import settings
import mimetypes
from django.db.models import Q




class Index(View):
    def get(self,request):        
        template = 'webapp/inicio.html'
        context = {}
        response = render(request,template, context)
        return response

Index = Index.as_view()


class addComment(View):
    def get(self,request):
        template = 'mainapp/dycss.css'
        context = {}
        response = render(request,template, context)
        return response

addComment = addComment.as_view()


class getComments(View):
    def get(self,request):
        template = 'mainapp/dycss.css'
        context = {}
        response = render(request,template, context)
        return response

getComments = getComments.as_view()



